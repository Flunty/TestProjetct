﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace TestProject
{
    class Program
    {
        static void Main(string[] args)
        {
            int command;
            while (true)
            {
                Console.WriteLine("Введите: ");
                Console.WriteLine("1 - Числа фибоначи");
                Console.WriteLine("2 - Конвертер температуры");
                Console.WriteLine("0 - Выход");
                Console.WriteLine("-1 - Очистить экран");
                try
                {
                    command = Convert.ToInt32(Console.ReadLine());
                }
                catch (Exception)
                {
                    Console.WriteLine("Введена неверная команда");
                    continue;
                }

                switch (command)
                {
                    case 1:
                        Console.WriteLine("Введите до какого чилса Фибоначи должен быть расчет");
                        try
                        {
                            int n = Convert.ToInt32(Console.ReadLine());
                            FibonachiGenerator(n).ToList().ForEach(x => Console.Write(x + " "));
                            Console.WriteLine();
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("Неверно введено число");
                            continue;
                        }
                        break;
                    case 2:
                        Console.WriteLine("Введите строку для перевода");
                        Console.WriteLine("Пример: ");
                        Console.WriteLine("1 F -> K");

                        string s = Console.ReadLine();
                        double temp = ConvertTemperature(s);

                        if (temp == int.MaxValue)
                        {
                            Console.WriteLine("Неверно указана строка");
                            continue;
                        }
                        else if (temp == int.MinValue)
                        {
                            Console.WriteLine("Неверно указана одна из размерностей");
                            continue;
                        }

                        Console.WriteLine(temp);
                        break;
                    case 0:
                        return;
                    case -1:
                        Console.Clear();
                        break;
                    default:
                        Console.WriteLine("Введена неверная команда");
                        break;
                }
            }
        }

        private static int[] FibonachiGenerator(int n)
        {
            int[] result = new int[n];

            result[0] = 1;
            result[1] = 1;

            for (int i = 2; i < n; i++)
            {
                result[i] = result[i - 1] + result[i - 2];
            }

            return result;
        }

        private static double ConvertTemperature(string s) // Я осознаю, что ошибки можно возвращать пробросом ошибки с текстом
        {
            string initiall, finalSystem, initialSystem;
            double temperatureNumber;

            Regex splitter = new Regex(" -> ");

            try
            {
                initiall = splitter.Split(s)[0];
                finalSystem = splitter.Split(s)[1];
            }
            catch (Exception)
            {
                return int.MaxValue;
            }

            try
            {
                temperatureNumber = Convert.ToDouble(initiall.Split(' ')[0]);
                initialSystem = initiall.Split(' ')[1];
            }
            catch (Exception)
            {
                return int.MaxValue;
            }

            switch (initialSystem)
            {
                case "C":
                    switch (finalSystem)
                    {
                        case "K":
                            return temperatureNumber + 273.15;
                        case "F":
                            return temperatureNumber * 1.8 + 32;
                        default:
                            return int.MinValue;
                    }
                case "K":
                    switch (finalSystem)
                    {
                        case "C":
                            return temperatureNumber - 273.15;
                        case "F":
                            return (temperatureNumber - 273.15) * 1.8 + 32;
                        default:
                            return int.MinValue;
                    }
                case "F":
                    switch (finalSystem)
                    {
                        case "K":
                            return (temperatureNumber - 32) / 5/9 + 273.15;
                        case "C":
                            return (temperatureNumber - 32) / 5/9;
                        default:
                            return int.MinValue;
                    }
                default:
                    return int.MinValue;
            }
        }
    }
}
